// @Monkhaus : https://youtu.be/XEZB-XbwihA?t=1054 ; but for Vuex

import { defineStore } from 'pinia'

export const useAuthStore = defineStore(
  'authStore',
  {
    state: () => ({
      token: "",
      isAuthenticated: false,

      // for fast frontend needs
      username: null,  // this is secondary (to display user in FE); while primary is the request.user identification in BE
      email: "",       // maybe empty preferable instead of null to be sure with md5(..)

      $api: null,  // constant, but no idea ho to handle it, it is from initializeAuth(api) and used later here
    }),
    actions: {
      initializeAuth (api) {
        this.$api = api
        const token = localStorage.getItem("auth_token")
        this.username = localStorage.getItem("auth_username")
        this.email = localStorage.getItem("auth_email")
        if (token) {
          this.setToken(token)
        } else {
          this.removeToken()
        }
      },
      setToken (token) {
        this.token = token
        this.isAuthenticated = true
        this.setAuthorization()
      },
      removeToken () {
        this.token = ""
        this.isAuthenticated = false
        this.username = ""
        this.setAuthorization()
      },
      setAuthorization () {
        const tokenString = this.token ? "Token " + this.token : ''
        this.$api.defaults.headers.common['Authorization'] = tokenString
      },
      setStorage (username) {
        this.username = username || null
        this.email = ""   // not null, more sure for md5(..)
        localStorage.setItem("auth_email", "")  // fetched in LogIn but with delay (2nd API call)
        localStorage.setItem("auth_username", this.username)
        localStorage.setItem("auth_token", this.token)
      },
      setMoreOnFE (data) {
        this.email = data.email || ""
        localStorage.setItem("auth_email", this.email)
      },
      logout () {
        this.removeToken()
        this.setStorage()
      }
    },
  }
)

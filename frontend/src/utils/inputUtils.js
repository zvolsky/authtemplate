const inputMixin = {
  methods: {
    getLabelColor () {
      return this.mandatory ? 'negative' : 'secondary'
    },
    getBgColor () {
      if (this.errorState === 2) {
        return 'red-5'
      } else if (this.errorState === 1) {
        return 'orange-2'
      } else {
        return ''
      }
    },
    scumbleError () {
      if (this.errorState === 2) {
        this.errorIcon = false
        this.errorState = 1
      }
    },
  },
}

export { inputMixin }

import { boot } from 'quasar/wrappers'
import axios from 'axios'
// import axiosRetry from 'axios-retry'

let dev = false
let baseUrl = 'http://127.0.0.1:8000'
if (location.port) {
  if (location.port === "4000") {    // :4000 for `quasar serve`
  } else {
    dev = true
  }
} else {
  baseUrl = location.origin
}

// axios.defaults.withCredentials = true
// axios.defaults.xsrfHeaderName = "X-CSRFTOKEN"
// axios.defaults.xsrfCookieName = "csrftoken"

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)

const api = axios.create({ baseURL: baseUrl })
// axiosRetry(axios, { retries: 3 })

export default boot(({ app }) => {
  app.config.globalProperties.$dev = dev

  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export { api }

const dsn = "https://3497d799e32a4a5aafa6c4ce67960bdb@sentry.comtel.fel.cvut.cz/11"

// (1)
// produces: ..from origin 'http://localhost:8080' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.
// maybe this can be repaired by Sentry server setting,
// see "CORS Attributtes.." or "Tunnel" at: https://docs.sentry.io/platforms/javascript/troubleshooting/

// import { boot } from "quasar/wrappers";
// import * as Sentry from "@sentry/vue";
// import { BrowserTracing } from "@sentry/tracing";

// export default boot(({ app, router }) => {
//   Sentry.init({
//     app,
//     dsn: dsn,
//     integrations: [
//       new BrowserTracing({
//         routingInstrumentation: Sentry.vueRouterInstrumentation(router),
//         tracingOrigins: ["localhost:8080", "production-url.com"],  // [..., regex]
//       }),
//     ],
//     trackComponents: true,
//     tracesSampleRate: 1.0,
//   })
// }) 

// (2) ..works
import { boot } from "quasar/wrappers";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";

export default boot(({ Vue }) => {
  Sentry.init({
    dsn: dsn,
    integrations: [
      new Integrations.Vue({ Vue, attachProps: true }),
      new Integrations.RewriteFrames({
        iteratee(frame) {
          // Strip out the query part (which contains `?__WB_REVISION__=**`)
          frame.abs_path = frame.abs_path.split("?")[0];
          return frame;
        },
      }),
    ],
  });
});

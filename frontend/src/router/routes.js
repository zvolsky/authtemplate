const routes = [
  {
    path: '/auth',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'log-in/:activated?', component: () => import('/src/pages/auth/TheLogIn.vue') },
      { path: 'profile', component: () => import('/src/pages/auth/TheUserProfile.vue') },
      { path: 'sign-up', component: () => import('/src/pages/auth/TheSignUp.vue') },
      { path: 'after-sign-up', component: () => import('/src/pages/auth/TheAfterSignUp.vue') },
      // { path: 'user-activate/:uid/:token', component: () => import('/src/pages/auth/TheUserActivateRedirect.vue') },
      //      see drf_3.py DJOSER['ACTIVATION_URL'] and comments in TheUserActivateRedirect.vue
      { path: 'resend-activation-mail/:activated?', component: () => import('/src/pages/auth/TheResendActivationMail.vue') },
      { path: 'reset-password-mail', component: () => import('/src/pages/auth/TheResetPasswordMail.vue') },
      { path: 'reset-password-new/:uid/:token', component: () => import('/src/pages/auth/TheResetPasswordNew.vue') },
      { path: 'set-password', component: () => import('/src/pages/auth/TheSetPassword.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('/src/pages/IndexPage.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes

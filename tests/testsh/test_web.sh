#!/bin/bash

# authtemplate> tests/testsh/test_web.sh


echo Create empty testing database ...

# https://stackoverflow.com/questions/876522/creating-a-copy-of-a-database-in-postgresql

cd $HOME/dj/authtemplate/authtemplate/
pg_dump -s -d authtemplate &>/dev/null > tests/testdb/authtemplate_empty.sql
psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_authtemplate' AND pid <> pg_backend_pid();" &>/dev/null
psql -c "drop database if exists test_authtemplate;" &>/dev/null
psql -c "create database test_authtemplate owner authtemplate;" &>/dev/null
psql -d test_authtemplate &>/dev/null < tests/testdb/authtemplate_empty.sql


echo Start backend with testing database ...

activate () {
  . .venv/bin/activate &>/dev/null
}
activate
pip install --upgrade pip setuptools &>/dev/null

cd backend/ &>/dev/null
TEST=1 ./manage.py migrate --fake &>/dev/null &>/dev/null
TEST=1 ./manage.py runserver &>/dev/null &
cd - &>/dev/null


echo Start frontend ...

cd frontend/ &>/dev/null
quasar dev &>/dev/null &
cd - &>/dev/null


echo Run web tests ...

cd tests/ &>/dev/null
yarn run codeceptjs
cd - &>/dev/null


# https://unix.stackexchange.com/questions/43527/kill-all-background-jobs

kill $(jobs -p) &>/dev/null
deactivate

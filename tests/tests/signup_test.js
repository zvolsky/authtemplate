Feature('signup')

Scenario('test signup', async ({ I }) => {
    const frontend = "http://localhost:9000"
    const username = "testuser40"
    const password = "aaaaaaZ4-"

    const tempMail = "https://etempmail.net/"
    I.amOnPage(tempMail)
    I.click("with Recommended Cookies")
    const email = await I.grabTextFrom("#email_id")
    // while (I.seeElement(".messages div")) {
    //     I.click(".messages div")
    //     I.click("Delete")
    // }

    I.amOnPage(frontend)
    I.click("Sign Up")
    I.fillField("Username", username)
    I.fillField("E-mail", email + "!")
    I.fillField("Password", password)
    I.fillField("Password (repeat)", "aaaaaa")
    I.click("[type='submit']")
    I.see("enter a valid email")
    I.fillField("E-mail", email)
    I.click("[type='submit']")
    I.see("Passwords are not identical")
    I.fillField("Password (repeat)", password)
    I.click("[type='submit']")
    I.see("User has been registered")

    I.amOnPage(tempMail)
    I.waitForElement(".messages div", 50)
    I.click(".messages div")
    // class="w-full flex flex-grow min-h-tm-groot-iframe"
    I.switchTo('iframe.min-h-tm-groot-iframe')  // Playwright related
    I.waitForElement({text: "http"})
    // why and how this works? It returns some Object without await (Promise)
    let activateUrl = await I.grabHTMLFrom({text: "http"})
    I.switchTo()  // Playwright related

    I.amOnPage(activateUrl)
    I.fillField("Username", username)
    I.fillField("Password", password)
    I.click("[type='submit']")
    I.click(username)
    I.click("Log out")
    I.waitUrlEquals(`${frontend.replace("localhost", "127.0.0.1")}/#/`)
})

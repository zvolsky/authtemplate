#!/bin/bash
pushd . > /dev/null
if [[ "$(pwd)" == *utils ]]
then
  cd ../
fi
if [[ "$(pwd)" == *backend ]]
then
  cd ../
fi

poetry install

echo
echo Collestatic..
echo
echo yes | backend/manage.py collectstatic > /dev/null

backend/manage.py migrate

popd > /dev/null

#!/bin/bash

# temporary, change (sometimes, not allways) the version and once uncomment the `sentry-cli releases new` command
#   TODO: read version from file and compare with file.bk if the `sentry-cli releases new` command is required

# download sentry-cli: https://docs.sentry.io/product/cli/installation/
# založit token (nechal jsem default permissiony tokenu): https://sentry.io/settings/account/api/auth-tokens/
# ~/.sentryclirc: [auth] token=...

pushd . > /dev/null
if [[ "$(pwd)" == *utils ]]
then
  cd ../
fi
if [[ "$(pwd)" == *backend ]]
then
  cd ../
fi
cd frontend/

quasar build --debug
# sentry-cli releases new 0.0.2 --org zvolsky --project authtemplate
sentry-cli releases files 0.0.2 upload-sourcemaps /home/mirek/dj/authtemplate/authtemplate/frontend/dist/spa/ --org zvolsky --project authtemplate

popd > /dev/null

psql -c "DROP DATABASE IF EXISTS authtemplate;"
psql -c "DROP ROLE IF EXISTS authtemplate;"

psql -c "CREATE ROLE authtemplate login password 'tezkeheslo';"
psql -c "CREATE DATABASE authtemplate WITH OWNER authtemplate TEMPLATE='template0' ENCODING='UTF-8' LOCALE='en_US.utf8';"  # COLLATION: "default"

psql -d authtemplate -c "CREATE EXTENSION postgis;"
psql -d authtemplate -c "CREATE EXTENSION unaccent;"  # need "django.contrib.postgres" +? need migration if introduced later? read ..
    # .. https://stackoverflow.com/questions/54071944/fielderror-unsupported-lookup-unaccent-for-charfield-or-join-on-the-field-not

psql -d authtemplate -c "CREATE COLLATION czech (provider = libc, LOCALE = 'cs_CZ.utf8');"
# see: "locale -a" & "SELECT * FROM pg_collation;"
# possible providers are libc, icu ; more examples:
    # psql -d authtemplate -c "CREATE COLLATION english (LOCALE = 'en_US.utf8');"  # LOCALE must differ from that in CREATE DATABASE
    # psql -d authtemplate -c "CREATE COLLATION unaccent (provider = icu, locale = 'und-u-ks-level1-kc-true', deterministic = false);"

# usage example:
    # from django.db.models.functions import Collate
    # Group.objects.filter(name__unaccent__startswith='C').order_by(Collate('name', 'czech'))
    # # Django 3.2+ : Collate() implemented by possibility make a template for Order By, see:
    # #     https://stackoverflow.com/questions/18935712/queryset-sorting-specifying-column-collation-for-django-orm-query

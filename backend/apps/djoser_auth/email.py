# https://stackoverflow.com/questions/70054175/how-to-customize-sent-emails-in-djoser
# SMTP server: https://www.gmass.co/blog/smtp-server-linux/

from djoser import email
from djoser import utils
from djoser.conf import settings
from django.contrib.auth.tokens import default_token_generator


class ActivationEmail(email.ActivationEmail):
    template_name = 'djoser_auth/ActivationEmail.html'

    def get_context_data(self):
        context = super().get_context_data()

        user = context.get("user")
        context["uid"] = utils.encode_uid(user.pk)
        context["token"] = default_token_generator.make_token(user)
        context["url"] = settings.ACTIVATION_URL.format(**context)
        return context


class ConfirmationEmail(email.ConfirmationEmail):
    template_name = 'djoser_auth/ConfirmationEmail.html'

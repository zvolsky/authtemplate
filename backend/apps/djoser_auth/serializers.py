# https://stackoverflow.com/questions/49095424/customize-the-djoser-create-user-endpoint

from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from djoser.serializers import UserCreateSerializer, UserSerializer

User = get_user_model()


class UserRegistrationSerializer(UserCreateSerializer):
    """
    This serializer prevents duplicate emails in User model because Djoser doesn't handle them properly
    """

    # class Meta(BaseUserRegistrationSerializer.Meta):
    #     pass
    #     # fields = ('url', 'id', 'email', 'name', 'last_name', 'account_address', 'password', )

    def validate_email(self, email):
        if self.Meta.model.objects.filter(email=email).exists():
            raise ValidationError(
                {"email": ["This email is used by another user."]},
                code="invalid email"
            )
        return email


class UserModelSerializer(UserSerializer):
    # UserSerializer has in default usage only these fields: id, username, email
    first_name = serializers.CharField(required=False, allow_blank=True, max_length=User.first_name.field.max_length)
    last_name = serializers.CharField(required=False, allow_blank=True, max_length=User.last_name.field.max_length)

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('first_name', 'last_name',)


class UserProfileSerializer(UserModelSerializer):
    def validate_email(self, email):
        if self.Meta.model.objects.filter(email=email).exclude(pk=self.instance.id).exists():
            raise ValidationError(
                {"email": ["This email is used by another user."]},
                code="invalid email"
            )
        return email

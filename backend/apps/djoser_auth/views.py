import requests

from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.shortcuts import redirect

from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from djoser.views import UserViewSet


User = get_user_model()

class TestMailView(APIView):
    # you need proper .env settings: email_host, email_port [int], email_user, email_password [false|true; but not False|True],
    # in core/urls.py uncomment /testmail
    # localhost:8000/testmail/
    #   sends a mail to console
    #   to really send a mail, comment temporarily out in dev_5.py: # EMAIL_BACKEND
    def get(self, request, format=None):
        send_mail(
            'Test message',
            'This is text of the test message.',
            'authtemplate@seznam.cz',
            ['zvolsky@seznam.cz'],
            fail_silently=False,
        )
        return Response({"ok": "yes"})


class UserActivationView(APIView):
    # "redirects" GET activate/ from activation email to POST activation/ (which is djoser's url); based on
    #   https://www.appsloveworld.com/django/100/48/djoser-user-activation-email-post-example
    #   https://stackoverflow.com/questions/59508580/how-to-handle-activation-url-with-django-djoser
    #   https://stackoverflow.com/questions/47154404/djoser-user-activation-email-post-example/47159919#47159919

    permission_classes = [AllowAny]

    def get(self, request, uid, token):
        web_url = request.build_absolute_uri('/')
        # activation/ is djoser's url, in opposite to activate/ which is added url for GET from email
        url = f"{web_url}api/v1/users/activation/"
        payload = {'uid': uid, 'token': token}

        result = requests.post(url, data=payload)  # here the Activation itself
        # return Response(f"{result.status_code}: {result.text}")

        if request.get_port() == '8000':  # dev
            web_url = web_url.replace(':8000', ':9000')

        if 200 <= result.status_code < 300:
            return redirect(f"{web_url}#/auth/log-in/activated_ok")
        elif result.status_code == 403:
            return redirect(f"{web_url}#/auth/log-in/stale_token")
        elif result.status_code == 400:
            return redirect(f"{web_url}#/auth/resend-activation-mail/invalid_user_or_token")
        else:
            return redirect(f"{web_url}#/auth/resend-activation-mail/activation_failed")


class UserInactiveView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        username = request.GET.get('u')
        if username:
            inactive = User.objects.filter(username=username, is_active=False).exists()
        else:
            inactive = False
        return Response({"inactive": inactive})

from django.apps import AppConfig


class DjoserAuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.djoser_auth'

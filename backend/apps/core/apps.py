from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "apps.core"  # https://www.alpharithms.com/django-improperlyconfigured-error-during-makemigrations-or-migrate-364618/

import os

from .third_4 import *

ALLOWED_HOSTS = ["*"]

#EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

INSTALLED_APPS += [
    "corsheaders",
]

# Shashank Gopikrishna @
#       https://stackoverflow.com/questions/34142182/insert-an-element-before-and-after-a-specific-element-in-a-list-of-strings
# MIDDLEWARE.insert(MIDDLEWARE.index("django.middleware.security.SecurityMiddleware") + 1,
#                               "debug_toolbar.middleware.DebugToolbarMiddleware")  # after
MIDDLEWARE.insert(
    MIDDLEWARE.index("django.middleware.common.CommonMiddleware"),
    "corsheaders.middleware.CorsMiddleware",
)  # before

# https://www.fatalerrors.org/a/cross-domain-csrf-problems-of-django-vue3.html
CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOWED_ORIGINS = CORS_ORIGIN_WHITELIST = (
    # # :9000, not :9000/
    "http://localhost:9000",   # for Vue project
    "http://127.0.0.1:9000",
    "http://localhost:4000",   # for Vue project
    "http://127.0.0.1:4000",
    #"http://127.0.0.1:8000",  # for Django  TODO: dev only??
)

# allow Basic Auth in curl/http(ie)
# sometimes, not sure when, Logout will fail with added Basic Auth:
#                       https://stackoverflow.com/questions/17665035/logout-not-working
REST_FRAMEWORK["DEFAULT_AUTHENTICATION_CLASSES"].append(
    "rest_framework.authentication.BasicAuthentication"
)

DDT = os.getenv("DDT", "0") == "1"
if DDT:
    INSTALLED_APPS.append("debug_toolbar")

    # MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")
    MIDDLEWARE.append("strawberry_django_plus.middlewares.debug_toolbar.DebugToolbarMiddleware"),
    
    INTERNAL_IPS = ("127.0.0.1",)

    DEBUG_TOOLBAR_CONFIG = {
        "INTERCEPT_REDIRECTS": False,
    }

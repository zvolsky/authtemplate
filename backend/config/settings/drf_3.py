# django rest framework and related settings

from .django_2 import *  # noqa F403

# Vue Auth implementation: https://www.youtube.com/watch?v=uqpM7WVTKI4 , Django: https://www.youtube.com/watch?v=PUzgZrS_piQ
REST_FRAMEWORK = {
    # https://djoser.readthedocs.io/en/latest/authentication_backends.html#authentication-backends
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.TokenAuthentication",             # for 'rest_framework.authtoken'
        # "rest_framework_simplejwt.authentication.JWTAuthentication",   # for 'rest_framework_simplejwt'

        # together with urls api-auth/ makes Web ui login of DRF working
        "rest_framework.authentication.SessionAuthentication",  # + Basic Auth in dev.py
    ],
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticatedOrReadOnly",),
    "DATE_INPUT_FORMATS": ["%d.%m.%Y", "%Y-%m-%d"],
    # Date format from Rest is ISO (yyyy-mm-ddThh:mm:...). In Vue/Quasar is made the locacalization, see List.vue:formatted
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}

DJOSER = {
    'USER_CREATE_PASSWORD_RETYPE': False,  # we don't send a password copy in Post data, the check is done on frontend only
    'SEND_ACTIVATION_EMAIL': True,
    'SEND_CONFIRMATION_EMAIL': True,
    #'ACTIVATION_URL': '#/auth/user-activate/{uid}/{token}/',   # frontend page which POST to api/v1/users/activate/{uid}/{token}/
    'ACTIVATION_URL': 'api/v1/users/activate/{uid}/{token}/',   # backend page which sends POST via requests lib
    'PASSWORD_RESET_CONFIRM_URL': '#/auth/reset-password-new/{uid}/{token}',
    # 'USERNAME_RESET_CONFIRM_URL': '#/username/reset/confirm/{uid}/{token}',
    'HIDE_USERS': True,
    'SERIALIZERS': {
        'user_create': 'apps.djoser_auth.serializers.UserRegistrationSerializer',
        'user': 'apps.djoser_auth.serializers.UserModelSerializer',
        'current_user': 'apps.djoser_auth.serializers.UserProfileSerializer',
            # for users/me/; it can differs from 'user' (like here: has additional validation); but MUST be explicitly given even if it's same
    },
    'EMAIL': {
        'activation': 'apps.djoser_auth.email.ActivationEmail',
        'confirmation': 'apps.djoser_auth.email.ConfirmationEmail',
        # 'password_reset': 'apps.djoser_auth.email.PasswordResetEmail',
        # 'password_changed_confirmation': 'apps.djoser_auth.email.PasswordChangedConfirmationEmail',
        # 'username_changed_confirmation': 'apps.djoser_auth.email.UsernameChangedConfirmationEmail',
        # 'username_reset': 'apps.djoser_auth.email.UsernameResetEmail',
    },
}

SPECTACULAR_SETTINGS = {
    "TITLE": "AuthTemplate",
    "DESCRIPTION": "Django & Rest Framework application",
    "VERSION": "1.0.0",
    "SERVE_INCLUDE_SCHEMA": False,
    # OTHER SETTINGS
}

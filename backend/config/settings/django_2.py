# rewritten django settings (+ few proprietary, see for "proprietary .." bellow)

import os
import json

from ..startproject_settings_1 import *  # noqa F403

with open(BASE_DIR / "config/.env") as f:
    env = json.loads(f.read())

# proprietary (non-django) settings
GIS = False
PROJECT_NAME = BASE_DIR.parent.stem

INSTALLED_APPS += [
    "django.contrib.postgres",
    "django_filters",
    "django_admin_listfilter_dropdown",  # auto-dropdown filter for admin
    "rest_framework",
    "rest_framework.authtoken",
    "djoser",
    "drf_spectacular",
    "django_extensions",
    "apps.core.apps.CoreConfig",  # apps/ ? see CoreConfig.name too
    "apps.djoser_auth.apps.DjoserAuthConfig",
    "strawberry_django_plus",  # needed here to find its template in integration with Django Debug Toolbar
]

SECRET_KEY = env["secret_key"]  # set this always in config/.env

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis"
        if GIS
        else "django.db.backends.postgresql",
        # "OPTIONS": {"options": env.get("db_options", {})},  # non-workig; what is better as the code bellow?
        "HOST": env.get("db_host", "localhost"),
        "NAME": ("test_" if os.environ.get("TEST") else "") + env.get("db_db", PROJECT_NAME),
        "USER": env.get("db_user", PROJECT_NAME),
        "PORT": env.get("db_port", 5432),
        "PASSWORD": env["db_password"],  # set this always in config/.env
    },
}
_db_options = env.get("db_options")
if _db_options:
    DATABASES["default"]["OPTIONS"] = _db_options
print(f'Database {DATABASES["default"]["NAME"]}')

# DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
#   not needed in Dj 4+, it is already BigAutoField (via cached_property, try cap.func(cap), it returns BigAutoField)

AUTH_USER_MODEL = "core.User"

TIME_ZONE = "Europe/Prague"

# to test SMTP in dev environment:
#   - temporary comment out django.core.mail.backends.console.EmailBackend in dev5.py
#   - uncomment /testmail in config/urls.py
#   - navigate to localhost:/testmail/
EMAIL_HOST = env["email_host"]
EMAIL_PORT = env["email_port"]
EMAIL_HOST_USER = env["email_user"]
EMAIL_HOST_PASSWORD = env["email_password"]
# Default is TLS. If explicit, use false or true (not: False|True) ; for smtp.seznam.cz port 465 use: true (ie. SSL)
if env.get("email_ssl", False):
    EMAIL_USE_TLS = False
    EMAIL_USE_SSL = True
else:
    EMAIL_USE_TLS = True
    EMAIL_USE_SSL = False
DEFAULT_FROM_EMAIL = "authtemplate@seznam.cz"

STATIC_URL = "static/"
STATIC_ROOT = BASE_DIR / "static/"
MEDIA_URL = "media/"
MEDIA_ROOT = BASE_DIR / "media/"

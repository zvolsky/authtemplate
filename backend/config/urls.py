from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

from apps.djoser_auth.views import TestMailView, UserActivationView, UserInactiveView

# from strawberry.django.views import GraphQLView
# # from rest_framework import  permissions, routers, serializers, viewsets
# # from rest_framework.authtoken import views as authviews


# import strawberry
# from strawberry.schema.config import StrawberryConfig


# # TODO Query, Mutation musí mít aspoň jeden parametr, pak lze zpřístupnit url graphql/

# # @strawberry.type
# # class Query():  # example: editor_schema.Query, map_services_schema.Query
# #     pass


# # @strawberry.type
# # class Mutation():  # example: editor_schema.Query, map_services_schema.Query
# #     pass


# # schema = strawberry.Schema(
# #     query=Query,
# #     mutation=Mutation,
# #     extensions=[],
# #     config=StrawberryConfig(auto_camel_case=False),
# # )


urlpatterns = [
    path("admin/", admin.site.urls),
    # path("graphql", GraphQLView.as_view(schema=schema)),

    # # probably not needed w djoser, because djoser has its own login and logout views (and more: password reset,..)
    # # path('api/v1/auth/', include('rest_framework.urls', namespace='rest_framework')),
    #                       # 'api-auth/' in docs | add REST framework's login and logout views
    # # path('api/v1/token/', authviews.obtain_auth_token),
    #                       # 'api-token-auth/' in docs | gives a token when a JSON username + password is POSTed
    # #     # requires json: username+password, respond json: token
    # path('api/v1/', include('djoser.urls')),
    # #path('api/v1/', include('djoser.urls.authtoken')),
    # #     # token/login, token/logout
    # #     # requires json: username+password, respond json: auth_token (jako obtain_auth_token,
    #                       url jsou token/login a token/logout)
    # path('api/v1/', include('djoser.urls.jwt')),          # url(r'^auth/',..) in docs | can url be the same?
    #      # jwt/create, jwt/refresh, jwt/verify
    # ## path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #                       for login/logout in native Rest UI
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]

if settings.SENTRY_DSN:

    def trigger_error(request):
        division_by_zero = 1 / 0  # noqa: F841

    urlpatterns += [
        path("sentry-debug/", trigger_error),
    ]

if "drf_spectacular" in settings.INSTALLED_APPS:
    from drf_spectacular.views import (
        SpectacularAPIView,
        SpectacularRedocView,
        SpectacularSwaggerView,
    )
    urlpatterns += [
        path("api-auth/", include("rest_framework.urls")),  # together with SessionAuthentication setting makes Web ui login of DRF working
        path("api/v1/users/inactive/", UserInactiveView.as_view()),  # is inactive? should we offer activation mail re-sending? ## before djoser.urls; because of ^users/(?P<id>[^/.]+)/$ in djoser.urls ?
        path("api/v1/", include("djoser.urls")),
        path("api/v1/", include("djoser.urls.authtoken")),
        re_path(r'^api/v1/users/activate/(?P<uid>[\w-]+)/(?P<token>[\w-]+)/$', UserActivationView.as_view()),  # GET, to be redirected to djoeer's POST

        path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
        path(
            "api/schema/redoc/",
            SpectacularRedocView.as_view(url_name="schema"),
            name="redoc",
        ),
        path(
            "api/schema/swagger-ui/",
            SpectacularSwaggerView.as_view(url_name="schema"),
            name="swagger-ui",
        ),
        path("testmail/", TestMailView.as_view(), name="test_mail"),
    ]
